# Project
Serenity BDD project using Cucumber - serenity-cucumber-project-candidate-2186170

## Project Brief Description
Serenity and Cucumber project caters for technical assignments from Home office. 
Covers UI and API automation testing exercises for the applications mentioned below

###Applications: 
Postcodes API: http://api.postcodes.io
UK VISA Web: https://www.gov.uk/check-uk-visa

### Project Directory Structure
```Gherkin
src
  + main
  + test
    + java
      + Test runner
      + API Stepdefinitions, steps and supporting code  
      + UI Stepdefinitions, pages and supporting code                  
    + resources
      + features                  Feature files
     + api_postcodes            Feature files for API tests - Postcodes API   
       + ui_uk_visa               Feature files for UI tests - UK VISA                

```

## Tool-suite
Serenity BDD
Cucumber BDD
Selenium WebDriver (Integrated with Serenity)
REST-assured
JUnit
Hamcrest
Maven
Git / GitLab


## Executing the tests
To run the test suite, you can either just run the `CucumberTestSuite` test runner class, or run `mvn verify` from the command line.

By default, the tests will run using Chrome. 
You can run them in Firefox by overriding the `driver` system property
```json
$ mvn clean verify -Dwebdriver.driver=firefox
```

## Test Results
The test results will be recorded in the `target/site/serenity` directory.

## Serenity configuration
Serenity uses the `serenity.conf` file in the `src/test/resources` directory to configure test execution options.  
Serenity restart browser flag has been employed in order to run the tests using the same browser for all scenarios at the feature level.
serenity.restart.browser.for.each = story

Serenity would capture screenshots for each UI action as below:
```java
serenity {
    take.screenshots = FOR_EACH_ACTION
}
```

## Webdriver configuration
The WebDriver configuration is managed entirely from `serenity.conf` file, as illustrated below:
```java
webdriver {
    driver = chrome
    autodownload = true
}
headless.mode = false
```

By default, the tests will run using Chrome. You can run them in Firefox by changing the `driver` property
The flag autodownload = true enables Serenity use WebDriverManager to download the WebDriver binaries automatically before the tests are executed.
And the headless mode is set to false by default. Setting it to true would run the tests in headless mode i.e., without opening the browser

Base URL for Selenium tests is defined using this property
webdriver.base.url



