Feature: Confirm whether a visa is required to visit the UK

  @CheckUKVisa_BasedOn_Country_Reason_And_PostReason
  Scenario Outline: Validate UK visa is required based on country, reason, and post-reason supplied
    Given I provide a nationality of "<Country>"
    And I select the "<Reason>"
    And I state "<PostReason>"
    When I submit the form
    Then I will be informed of a "<Decision>"

    Examples: 
      | Country | Reason  | PostReason           | Decision                                 |
      | Japan   | Study   | longer than 6 months | You’ll need a visa to study in the UK  |
      | Japan   | Study   | 6 months or less     | You do not need a visa to come to the UK |
      | Russia  | Tourism | No                   | You’ll need a visa to come to the UK   |
      | Russia  | Study   | longer than 6 months | You’ll need a visa to study in the UK  |
      | Russia  | Study   | 6 months or less     | You’ll need a visa to study in the UK  |
