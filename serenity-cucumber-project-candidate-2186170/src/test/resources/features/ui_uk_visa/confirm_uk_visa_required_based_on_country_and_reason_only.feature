Feature: Confirm whether a visa is required to visit the UK

  @CheckUKVisa_BasedOn_Country_And_Reason
  Scenario Outline: Validate UK visa is required based on country, reason (NO post-reason required)
    Given I provide a nationality of "<Country>"
    And I select the "<Reason>"
    Then I will be informed of a "<Decision>"

    Examples: 
      | Country | Reason  | Decision                                                  |
      | Japan   | Tourism | You do not need a visa if you’re staying up to 6 months |
