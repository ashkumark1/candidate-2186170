Feature: Query a postcode

  @QueryPostCode_CheckStatusCode 
  Scenario Outline: Query a postcode and Assert Status Code is 200
    Given the Postcodes API
    When I query a "<post code>"
    Then I expect a "<status code>"

    Examples: 
      | post code | status code |
      | SW1P4JA   |         200 |
      | SW14JA    |         404 |
