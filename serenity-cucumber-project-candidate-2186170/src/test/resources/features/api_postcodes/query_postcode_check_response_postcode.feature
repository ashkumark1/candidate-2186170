Feature: Query a postcode

  @QueryPostCode_CheckResponsePostCode
  Scenario Outline: Query a postcode and Check Response Post Code
    Given the Postcodes API
    When I query a "<post code>"
    Then I expect the queried "<post code>" in the response

    Examples: 
      | post code |
      | SW1P 4JA  |
