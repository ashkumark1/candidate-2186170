package uk.gov.homeoffice.technical.assignment.api.constants;

/**
 * Stores the environment variables and 
 * path information for Postcodes API service.
 * 
 * @author Ashok
 */
public enum PostCodesAPI_Constants {

	// GET http://api.postcodes.io/postcodes/SW1P4JA
	
    BASE_URI("http://api.postcodes.io"),    
	BASE_PATH("/postcodes/"),
	
	RESPONSE_PATH_POSTCODE("result.postcode");
	
    private String value;

    PostCodesAPI_Constants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}