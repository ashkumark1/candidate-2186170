package uk.gov.homeoffice.technical.assignment.ui.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

/**
 * This class covers actions performed on the ConfirmationPage
 *  
 * @author Ashok
 *
 */
public class CheckUkVisa_ConfirmationPage extends PageObject{

	@FindBy(css = ".result-body")
	private WebElementFacade resultBody;

	public WebElementFacade getResultBody() {
		return resultBody;
	}

	/**
	 * Steps
	 **/
	@Step("Step for getting decision text")
	public String getDecisionText() {	
		waitFor(getResultBody());
		return getResultBody().getTextContent();
	}

}