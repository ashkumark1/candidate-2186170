package uk.gov.homeoffice.technical.assignment.ui.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

/**
 * This class covers various behaviours based on the reason selected for a country
 * Examples: For some countries it shows:
 *  Duration - longer than 6 months, etc
 * Or
 *  Accompanied By - "Will you be travelling with or visiting either your partner or a family member in the UK?"
 *  
 * @author Ashok
 *
 */
public class CheckUkVisa_SelectPostReasonPage extends PageObject {
	@FindBy(xpath = "//*[contains(text(),'6 months or less')]")
	private WebElementFacade radioOptionSixMonthsOrLess;
	
	@FindBy(xpath = "//*[contains(text(),'longer than 6 months')]")
	private WebElementFacade radioOptionOverSixMonths;
	
	@FindBy(xpath = "//*[contains(text(),'Yes')]")
	private WebElementFacade radioOptionYes;
	
	@FindBy(xpath = "//*[contains(text(),'No')]")
	private WebElementFacade radioOptionNo;
    
	public WebElementFacade getRadioOptionSixMonthsOrLess() {
		return radioOptionSixMonthsOrLess;
	}

	public WebElementFacade getRadioOptionOverSixMonths() {
		return radioOptionOverSixMonths;
	}
	
	public WebElementFacade getRadioOptionYes() {
		return radioOptionYes;
	}

	public WebElementFacade getRadioOptionNo() {
		return radioOptionNo;
	}

	/**
	 * Steps
	 **/
	@Step("Step for Selecting Post Reason")
	public CheckUkVisa_SelectPostReasonPage selectRadioPostReason(String postReason) {		
		switch(postReason)
	     {
	        case "6 months or less":
	        	getRadioOptionSixMonthsOrLess().click();
	        	break;
	        case "longer than 6 months":
	        	getRadioOptionOverSixMonths().click();
	        	break;
	        case "Yes":
	        	getRadioOptionYes().click();
	        	break;
	        case "No":
	        	getRadioOptionNo().click();
	        	break;
	      }
		
		return this;
	}
	
}