package uk.gov.homeoffice.technical.assignment.api.steps;

import static io.restassured.RestAssured.given;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import uk.gov.homeoffice.technical.assignment.api.constants.PostCodesAPI_Constants;

import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;

/**
 * Stores the Generic Steps pertaining to REST-assured library.
 * 
 * @author Ashok
 */
public class QueryPostcodes_Generic_Steps {

	protected RequestSpecification requestSpec;
	protected ResponseSpecification responseSpec;
	protected Response response;
	
	@Step
	public void setRequestSpecBuilder() {
		requestSpec = new RequestSpecBuilder().
						  setBaseUri(PostCodesAPI_Constants.BASE_URI.getValue()).
						  setAccept(ContentType.JSON).
						  setContentType(ContentType.JSON).
					      build();
		
		Serenity.setSessionVariable("requestSpec").to(requestSpec);
	}
	
	@Step
	public void setResponseSpecBuilder() {
		responseSpec = new ResponseSpecBuilder().
						   expectContentType(ContentType.JSON).						   
						   build();
		
		Serenity.setSessionVariable("responseSpec").to(responseSpec);
	}

	@Step
	public void specifyRequest() {
		requestSpec = given().
						contentType(ContentType.JSON).
						accept(ContentType.JSON).
						spec(requestSpec);
	}

	@Step
	public void checkStatusCode(String expectedStatusCode) {		
		response = Serenity.sessionVariableCalled("response");
		
		responseSpec = Serenity.sessionVariableCalled("responseSpec");
		
		response.
				then().
					assertThat().spec(responseSpec).
				and().
					assertThat().statusCode(is(equalTo(Integer.parseInt(expectedStatusCode))));
	}
}
