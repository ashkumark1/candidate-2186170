package uk.gov.homeoffice.technical.assignment.api.stepdefinitions;

import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import uk.gov.homeoffice.technical.assignment.api.constants.PostCodesAPI_Constants;
import uk.gov.homeoffice.technical.assignment.api.steps.QueryPostcodes_PostCodeAPI_Specific_Steps;

public class QueryPostcode_Check_PostCodeInResponse_StepDefinitions {	
	
	@Steps
	QueryPostcodes_PostCodeAPI_Specific_Steps queryPostcodesSteps;
	
	@Then("I expect the queried {string} in the response")
	public void i_expect_the_queried_in_the_response(String postcode) {
		queryPostcodesSteps.checkQueriedPostCode_IsPresentInTheResponse(PostCodesAPI_Constants.RESPONSE_PATH_POSTCODE.getValue(), postcode);
	}
}
