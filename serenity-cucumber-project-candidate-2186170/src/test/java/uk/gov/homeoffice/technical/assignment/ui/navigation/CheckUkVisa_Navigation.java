package uk.gov.homeoffice.technical.assignment.ui.navigation;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

/**
 * This class captures common navigation steps across pages
 * 
 * @author Ashok
 *
 */
public class CheckUkVisa_Navigation extends PageObject {
	
	@FindBy(xpath = "//*[contains(text(),'Next step')]")
	private WebElementFacade nextStepButton;
    
	public WebElementFacade getNextStepButton() {
		return nextStepButton;
	}
	
	@Step("Step for Navigation - Next Step")
	public void clickNextStep() {
		waitFor(getNextStepButton());
		getNextStepButton().click();
	}
}
