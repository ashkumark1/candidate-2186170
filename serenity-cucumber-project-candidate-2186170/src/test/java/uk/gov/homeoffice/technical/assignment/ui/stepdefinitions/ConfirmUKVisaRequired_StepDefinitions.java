package uk.gov.homeoffice.technical.assignment.ui.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import uk.gov.homeoffice.technical.assignment.ui.navigation.CheckUkVisa_Navigation;
import uk.gov.homeoffice.technical.assignment.ui.pages.CheckUkVisa_ConfirmationPage;
import uk.gov.homeoffice.technical.assignment.ui.pages.CheckUkVisa_SelectNationalityPage;
import uk.gov.homeoffice.technical.assignment.ui.pages.CheckUkVisa_SelectPostReasonPage;
import uk.gov.homeoffice.technical.assignment.ui.pages.CheckUkVisa_SelectReasonPage;
import uk.gov.homeoffice.technical.assignment.ui.pages.CheckUkVisa_StartPage;

import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;

public class ConfirmUKVisaRequired_StepDefinitions {
	
	@Steps
	CheckUkVisa_Navigation checkUkVisa_Navigation;
	
	@Steps
	CheckUkVisa_StartPage checkUkVisa_StartPage;
	
	@Steps
	CheckUkVisa_SelectNationalityPage checkUkVisa_SelectNationalityPage;
	
	@Steps
	CheckUkVisa_SelectReasonPage checkUkVisa_SelectReasonPage;
	
	@Steps
	CheckUkVisa_SelectPostReasonPage checkUkVisa_SelecttPostReasonPage;
	
	@Steps
	CheckUkVisa_ConfirmationPage checkUkVisa_ConfirmationPage;
	
	@Given("I provide a nationality of {string}")
	public void i_provide_a_nationality_of(String country) throws InterruptedException {
		// Open the webdriver browser to the base URL
		checkUkVisa_StartPage.open();	
		
		// Start now
		checkUkVisa_StartPage.clickStartNow();
		
		// Select country & click next
		checkUkVisa_SelectNationalityPage.selectCountry(country);
		checkUkVisa_Navigation.clickNextStep();
	}

	@And("I select the {string}")
	public void i_select_the_reason(String reason) {
		// Select Radio button based on the reason & click next
		checkUkVisa_SelectReasonPage.selectRadioBasedOnReason(reason);
		checkUkVisa_Navigation.clickNextStep();
	}	

	@And("I state {string}")
	public void i_state_post_reason(String postReason) {
		// Select Radio button based on the post reason provided
		checkUkVisa_SelecttPostReasonPage.selectRadioPostReason(postReason);
	}

	@When("I submit the form")
	public void i_submit_the_form() {
		checkUkVisa_Navigation.clickNextStep();
	}

	@Then("I will be informed of a {string}")
	public void i_will_be_informed(String decision) {
		String actual = checkUkVisa_ConfirmationPage.getDecisionText();
		
		System.out.println("Actual: " + actual);
		System.out.println("Expected: " + decision);
		
	    assertThat(actual, containsString(decision));
	}
	
}
