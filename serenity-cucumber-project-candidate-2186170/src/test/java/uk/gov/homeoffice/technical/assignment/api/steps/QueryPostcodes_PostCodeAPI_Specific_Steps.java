package uk.gov.homeoffice.technical.assignment.api.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

/**
 * Inherits the members of Generic Steps pertaining to REST-assured library.
 * Also Specific Steps are defined for working with Postcodes API service.
 * 
 * @author Ashok
 */
public class QueryPostcodes_PostCodeAPI_Specific_Steps extends QueryPostcodes_Generic_Steps {

	@Step
	public void queryByPostCode(String basePath, String postCode) {		
		response = requestSpec.
						when().
							get(basePath + postCode);
		
		Serenity.setSessionVariable("response").to(response);
		
		System.out.println("Response: " + response.getBody().prettyPrint());
	}
	
	@Step
	public void checkQueriedPostCode_IsPresentInTheResponse(String path, String postCode) {
		response = Serenity.sessionVariableCalled("response");

		String actualPostCode = response.jsonPath().getString(path);
		
		System.out.println("Actual Post Code: " + actualPostCode);
		System.out.println("Expected Post Code: " + postCode);

		assertThat(actualPostCode, is(equalTo(postCode)));
	}
}
