package uk.gov.homeoffice.technical.assignment.ui.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

/**
 * This class covers actions performed on the SelectReasonPage
 *  
 * @author Ashok
 *
 */
public class CheckUkVisa_SelectReasonPage extends PageObject {	
	
	@FindBy(xpath = "//*[contains(text(),'Tourism')]")
	private WebElementFacade radioOptionTourism;
	
	@FindBy(xpath = "//*[contains(text(),'Study')]")
	private WebElementFacade radioOptionStudy;
    
	public WebElementFacade getRadioOptionTourism() {
		return radioOptionTourism;
	}

	public WebElementFacade getRadioOptionStudy() {
		return radioOptionStudy;
	}

	/**
	 * Steps
	 **/
	@Step("Step for Selecting Reason")
	public CheckUkVisa_SelectReasonPage selectRadioBasedOnReason(String reason) {		
		switch(reason)
	     {
	        case "Study":
	        	getRadioOptionStudy().click();
	        	break;
	        case "Tourism":
	        	getRadioOptionTourism().click();
	        	break;
	        default:
	        	getRadioOptionStudy().click();
	        	break;
	      }
		
		return this;
	}
	
}