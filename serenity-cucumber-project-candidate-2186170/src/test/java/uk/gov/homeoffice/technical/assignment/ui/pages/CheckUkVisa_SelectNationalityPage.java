package uk.gov.homeoffice.technical.assignment.ui.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

/**
 * This class covers actions performed on the SelectNationalityPage
 *  
 * @author Ashok
 *
 */
public class CheckUkVisa_SelectNationalityPage extends PageObject{
	
	@FindBy(xpath = "//*[@id='response']")
	private WebElementFacade selectCountryDropDown;
    
	public WebElementFacade getSelectCountryDropDown() {
		return selectCountryDropDown;
	}

	/**
	 * Steps
	 **/
	@Step("Step for Selecting Country")
	public CheckUkVisa_SelectNationalityPage selectCountry(String country) {		
		waitFor(getSelectCountryDropDown());
		getSelectCountryDropDown().selectByVisibleText(country);
		
		return this;
	}
	
}