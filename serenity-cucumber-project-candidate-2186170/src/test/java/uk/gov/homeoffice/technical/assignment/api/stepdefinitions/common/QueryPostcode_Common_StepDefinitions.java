package uk.gov.homeoffice.technical.assignment.api.stepdefinitions.common;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import uk.gov.homeoffice.technical.assignment.api.constants.PostCodesAPI_Constants;
import uk.gov.homeoffice.technical.assignment.api.steps.QueryPostcodes_PostCodeAPI_Specific_Steps;

public class QueryPostcode_Common_StepDefinitions {
	
	@Steps
	QueryPostcodes_PostCodeAPI_Specific_Steps queryPostcodesSteps;
	
	@Given("the Postcodes API")
	public void the_postcodes_api() {
		queryPostcodesSteps.setRequestSpecBuilder();
		queryPostcodesSteps.setResponseSpecBuilder();
		queryPostcodesSteps.specifyRequest();
	}

	@When("I query a {string}")
	public void i_query_the_api(String postcode) {
		queryPostcodesSteps.queryByPostCode(PostCodesAPI_Constants.BASE_PATH.getValue(), postcode);
	}
	
}
