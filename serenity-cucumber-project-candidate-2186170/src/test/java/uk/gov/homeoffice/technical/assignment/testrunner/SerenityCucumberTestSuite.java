package uk.gov.homeoffice.technical.assignment.testrunner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features",
        glue = {"uk.gov.homeoffice.technical.assignment.api.stepdefinitions",
        		"uk.gov.homeoffice.technical.assignment.ui.stepdefinitions"}
)
public class SerenityCucumberTestSuite {
	
}
