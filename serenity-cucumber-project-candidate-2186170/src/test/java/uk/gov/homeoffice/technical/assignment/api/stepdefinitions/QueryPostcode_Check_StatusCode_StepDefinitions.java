package uk.gov.homeoffice.technical.assignment.api.stepdefinitions;

import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import uk.gov.homeoffice.technical.assignment.api.steps.QueryPostcodes_PostCodeAPI_Specific_Steps;

public class QueryPostcode_Check_StatusCode_StepDefinitions {
	
	@Steps
	QueryPostcodes_PostCodeAPI_Specific_Steps queryPostcodesSteps;

	@Then("I expect a {string}")
	public void the_status_code_is(String expectedStatusCode) {
		queryPostcodesSteps.checkStatusCode(expectedStatusCode);
	}
}
