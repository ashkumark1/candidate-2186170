package uk.gov.homeoffice.technical.assignment.ui.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

/**
 * This class covers actions performed on the StartPage
 *  
 * @author Ashok
 *
 */
public class CheckUkVisa_StartPage extends PageObject {
	
	@FindBy(xpath = "//*[@id='get-started']/a")
	private WebElementFacade startNowButton;
	
	public WebElementFacade getStartNowButton() {
		return startNowButton;
	}

	/**
	 * Steps	 
	 **/
	@Step("Step for Start Now")
	public CheckUkVisa_SelectNationalityPage clickStartNow() {
		waitFor(getStartNowButton());
		getStartNowButton().click();
		
		return new CheckUkVisa_SelectNationalityPage();
	}
}

